require 'spec_helper'

describe Comment do

  before(:each) do
    @user = User.create(login:"U1", email:"d@@d")
    @episode = Episode.create(title:"ep1")
    @comment = Comment.create(title: "Koment1", user: @user)
  end

  it "should exist" do
    @comment.should_not == nil
  end

  it "should have user" do
    @comment.user.should_not == nil
  end



end