require 'spec_helper'

describe User do

  before(:each) do
    @user = User.create(login:"U1", email:"d@@d")
  end

  after(:each) do
    @user = User.destro
  end

  it "should exist" do
     @user.should_not == nil
  end

  it "should have id" do
    @user.id.should_not == nil
  end

  it "should have login" do
     @user.login.should_not be_empty
  end

  it "should have email" do
    @user.email.should_not be_empty
  end



end