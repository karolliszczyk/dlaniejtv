class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  belongs_to :user
  validates :user_id, presence: true


  def self.can_edit?(user)
    self.user ==user
  end



end
