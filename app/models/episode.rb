class Episode < ActiveRecord::Base
  belongs_to :program
  has_many :comments,  :as => :commentable

  def self.find_episodes(program_id)
    if program_id
      Episode.where(:program_id => program_id)
    else
      Episode.all
    end
  end

end
