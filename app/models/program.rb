class Program < ActiveRecord::Base
  belongs_to :admin
  has_many :episodes
  has_attached_file :image, :styles => { :medium => "100x100>", :thumb => "100x100>" }

end
