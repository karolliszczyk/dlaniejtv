class CommentsController < ApplicationController
  before_filter :load_commentable, except: [:index]

  def index
    @comments = Comment.all

  end

  def new
    @comment = @commentable.comments.build
  end

  def create
    @comment = @commentable.comments.build comment_params
    @comment.user = current_user
    if @comment.save
      redirect_to @commentable, notice: "Comment created."
    else
      render :new
    end

  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end

   def load_commentable
    resource, id = request.path.split('/')[1, 2]
   @commentable = resource.singularize.classify.constantize.find(id)
   end




  def comment_params
    params.require(:comment).permit(:title,:text,:episode_id, :comment_id)
  end
  # def load_commentable
  #   klass = [Article, Photo, Event].detect { |c| params["#{c.name.underscore}_id"] }
  #   @commentable = klass.find(params["#{klass.name.underscore}_id"])
  # end
end
