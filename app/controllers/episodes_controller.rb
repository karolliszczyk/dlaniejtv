class EpisodesController < ApplicationController
  before_action :allow_iframe_requests
  before_action :set_episode, :only =>[:show,:edit, :update, :destroy]

  def index
    @episodes = Episode.find_episodes params[:program_id]
  end

  def show
    @episodes = Episode.find(params[:id])
    @commentable = @episode
    @comments = @commentable.comments
    @comment = Comment.new
  end

  def edit

  end

  def new
    @episode = Episode.new
  end

  def create
    @episode = Episode.new episode_params
    respond_to do |format|
      if @episode.save
        format.html { redirect_to @episode, notice: 'Episode was successfully created.' }
        format.json { render action: 'show', status: :created, location: @program }
      else
        format.html { render action: 'new' }
        format.json { render json: @episode.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_episode
    @episode = Episode.find(params[:id])
  end

  def episode_params
    params.require(:episode).permit(:title, :description, :admin_id, :url, :program_id)
  end

  def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end


  def update
    respond_to do |format|
      if @episode.update(episode_params)
        format.html { redirect_to @episode, notice: 'Episode was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @episode.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @episode.destroy
    respond_to do |format|
      format.html { redirect_to programs_url }
      format.json { head :no_content }
    end
  end
end
