class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :name
      t.string :description
      t.references :admin, index: true

      t.timestamps
    end
  end
end
