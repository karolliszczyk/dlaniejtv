class AddAttachmentImageToPrograms < ActiveRecord::Migration
  def self.up
     add_attachment :programs, :image
  end

  def self.down
    remove_attachment :programs, :image
  end
end
