class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :title
      t.string :description
      t.string :url
      t.references :programs, index: true

      t.timestamps
    end
  end
end
