class FixColumProgramId < ActiveRecord::Migration
  def change
    rename_column :episodes,:programs_id,:program_id
  end
end
