class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :description
      t.string :url
      t.references :topics, index: true

      t.timestamps
    end
  end
end
