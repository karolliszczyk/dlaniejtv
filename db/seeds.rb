# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Article.destroy_all
Episode.destroy_all
User.destroy_all

a1 = Article.create(title:"A1", description:"ddsdsdsd")
a2 = Article.create(title:"A2", description:"ddsdsdsd")

e1 = Episode.create(title:"E1", description:"ddsdsdsd")
e2 = Episode.create(title:"E2", description:"ddsdsdsd")

u1 = User.create( email:"d@o2.pl")
u2 = User.create( email:"q@wer.pl")


e1.comments.build(title: "KOMENT1", user:u1).save!
e2.comments.build(title: "KOMENT2", user:u1).save!
a1.comments.build(title: "KOMENT3", user:u2).save!
a2.comments.build(title: "KOMENT4", user:u2).save!


#Comment.create(title: "Koment1", commentable_type: "episodes", commentable_id: 1, user: u1)
#Comment.create(title: "Koment2", articles: a2, user: u1)
#Comment.create(title: "Koment3", episodes: e2, user: u2)
#Comment.create(title: "Koment4", articles: a1, user: u2)